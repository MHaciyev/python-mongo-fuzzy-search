class LocationResource:
    @staticmethod
    def single(data):
        return {
            "city": data.city,
            "street": data.street,
            "street_number": data.street_number,
            "coordinates": f"{data.location['coordinates'][0]}, {data.location['coordinates'][1]}"
        }

    @staticmethod
    def collect(data):
        return list(map(LocationResource.single, data))
