from flask import Blueprint
from controllers.location import locations

location = Blueprint('location', __name__)

location.route('/', methods=['GET'])(locations)
