from .location_seeder import LocationSeeder


class Seeder:
    seeders = [LocationSeeder()]

    def run(self):
        for seed in self.seeders:
            seed.seed()
