import click
from flask.cli import with_appcontext
from seeders.seeder import Seeder


@click.command(name='seed-db')
@with_appcontext
def seed():
    Seeder().run()
