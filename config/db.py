import os

from flask_mongoengine import MongoEngine


def configure_db(app):
    try:
        db = MongoEngine()
        app.config['MONGODB_SETTINGS'] = {
            'db': os.getenv('MONGO_DB'),
            'host': os.getenv('MONGO_HOST'),
            'port': int(os.getenv('MONGO_PORT'))
        }
        db.init_app(app)
    except Exception as e:
        print(e)
